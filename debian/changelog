ruby-mousetrap-rails (1.4.6-8) unstable; urgency=medium

  [ Debian Janitor ]
  * Update watch file format version to 4.

  [ Pirate Praveen ]
  * Fix autopkgtest dependencies (remove ruby-coffee-rails)
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Wed, 08 Feb 2023 17:16:49 +0100

ruby-mousetrap-rails (1.4.6-7) unstable; urgency=high

  * Team upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Cédric Boutillier ]
  * [ci skip] Update team name
  * [ci skip] Add .gitattributes to keep unwanted files out of the
    source package

  [ Utkarsh Gupta ]
  * Reduce autopkgtest dependencies by embedding a dummy rails app.
  * Remove obsolete Needs-recommends.
  * Bump Standards-Version to 4.5.1 and dh-compat to 13.

 -- Utkarsh Gupta <utkarsh@debian.org>  Tue, 23 Feb 2021 17:19:10 +0530

ruby-mousetrap-rails (1.4.6-6) unstable; urgency=medium

  * debian/tests/control: change need-recommends to needs-recommends

 -- Pirate Praveen <praveen@debian.org>  Mon, 06 Jun 2016 11:08:33 +0530

ruby-mousetrap-rails (1.4.6-5) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Team upload.
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Christian Hofstaedtler ]
  * Limit testsuite to need-recommends.
    Thanks to Matthias Klose <doko@debian.org>

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 17 Mar 2016 21:50:19 +0000

ruby-mousetrap-rails (1.4.6-4) unstable; urgency=medium

  * Stop providing libjs-mousetrap (Closes: #804116)
    libjs-mousetrap is now available as a separate package
  * Add smoke test for assets

 -- Pirate Praveen <praveen@debian.org>  Tue, 12 Jan 2016 16:47:39 +0530

ruby-mousetrap-rails (1.4.6-3) unstable; urgency=medium

  * Install mousetrap.js also in libjs-mousetrap

 -- Pirate Praveen <praveen@debian.org>  Mon, 02 Nov 2015 05:30:49 +0530

ruby-mousetrap-rails (1.4.6-2) unstable; urgency=medium

  * Add section 'web' for libjs-mousetrap (Closes: #792244)
  * Install assets to /usr/share

 -- Pirate Praveen <praveen@debian.org>  Mon, 02 Nov 2015 05:04:07 +0530

ruby-mousetrap-rails (1.4.6-1) unstable; urgency=medium

  * Initial release (Closes: #790850,#703065)

 -- Pirate Praveen <praveen@debian.org>  Mon, 06 Jul 2015 17:32:57 +0530
